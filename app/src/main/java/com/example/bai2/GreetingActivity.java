package com.example.bai2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class GreetingActivity extends AppCompatActivity {
    private static GreetingActivity ins;
    private EditText edtName=null;
    private Button btnGreet=null;
    private TextView tvGreeting=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greeting);
        ins=GreetingActivity.this;
        anhxa();
        setOnClickListener();
    }
    public static GreetingActivity getInstance(){
        return ins;
    }
    private void anhxa() {
        if(edtName==null)
            edtName=findViewById(R.id.edtName);
        if(btnGreet==null)
            btnGreet=findViewById(R.id.btnGreet);
        if(tvGreeting==null)
            tvGreeting=findViewById(R.id.txtGreeting);
    }
    private void setOnClickListener(){
        if (btnGreet==null)
            anhxa();
        btnGreet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name =edtName.getText().toString();
                Intent intent=new Intent(GreetingActivity.this,GreetingReceiver.class);
                Bundle bundle=new Bundle();
                bundle.putString("name",name);
                intent.putExtra("greetapp",bundle);
                intent.setAction("vn.com.example.GREET");
                sendBroadcast(intent);
            }
        });
    }
    public void updateResult(String name){
        if(tvGreeting==null)
            anhxa();
        tvGreeting.setText("Hello: "+name);
    }
}
