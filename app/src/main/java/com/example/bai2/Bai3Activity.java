package com.example.bai2;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Bai3Activity extends AppCompatActivity {

 private TextView txtkqsms=null;
    private static Bai3Activity ins;
    private static final int MY_PERMISSIONS_REQUEST_RECEIVE_SMS = 9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai3);
        ins=Bai3Activity.this;
        anhxa();
        requestsms();
        Intent intent=new Intent(Bai3Activity.this,SmsActivity.class);
        //intent.setAction("android.provider.Telephony.SMS_RECEIVED");
        sendBroadcast(intent);


    }

    private void anhxa() {
        if(txtkqsms==null)
        txtkqsms=findViewById(R.id.txtsmskq);
    }
    public static Bai3Activity getInstance(){
        return ins;
    }
    public void updateResult(String phonesms){
        if(txtkqsms==null)
            anhxa();

        txtkqsms.setText("Messinger: "+phonesms);
    }
    protected void requestsms(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECEIVE_SMS)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECEIVE_SMS},
                        MY_PERMISSIONS_REQUEST_RECEIVE_SMS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }

}
